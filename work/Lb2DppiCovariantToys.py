# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf

import sys, os
sys.path.append("../")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import TH1F, TH2F, TCanvas, TFile

from TensorFlowAnalysis import *

if __name__ == "__main__" : 

  mlb = 5.620
  md  = 1.865
  mpi = 0.139
  mp  = 0.940

  phsp = Baryonic3BodyPhaseSpace(md, mp, mpi, mlb, mabrange = (0., 3.) )

  def model(x) : 

    m2dp  = phsp.M2ab(x)
    m2ppi = phsp.M2bc(x)

    p4d, p4p, p4pi = phsp.FinalStateMomenta(m2dp, m2ppi, 0., 0., 0.)
    p4lb = p4d + p4p + p4pi

    spinor_p  = DiracSpinors(1, p4p,  mp)
    spinor_lb = DiracSpinors(1, p4lb, mlb)

    density = Const(0.)

    spin_r = 3
    parity_r = -1
    parity_d = 1

    for pol_lb in range(len(spinor_lb)) : 
      for pol_p in range(len(spinor_p)) : 
        ampl = CovariantBaryonDecayAmplitude(p4p, p4d, p4pi, p4lb, spinor_p[pol_p], spinor_lb[pol_lb], spin_r, parity_r, parity_d)
        density += Density(ampl)

    return density

  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  tf.set_random_seed(1)
  sess = tf.Session()

  init = tf.global_variables_initializer()
  sess.run(init)

  majorant = EstimateMaximum(sess, model(data_ph), data_ph, sess.run( phsp.UniformSample(1000000) ) )*1.5
  print "Maximum = ", majorant
  data_sample = RunToyMC( sess, model(data_ph), data_ph, phsp, 100000, majorant, chunk = 100000)

  f = TFile.Open("toy.root", "RECREATE")
  FillNTuple("toy", data_sample, ["m2dp", "m2ppi" ])
  f.Close()

  h1 = TH2F("h1", "", 100, phsp.minab-0.2, phsp.maxab+0.2, 100, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
  h2 = TH2F("h2", "", 100, phsp.minab-0.2, phsp.maxab+0.2, 100, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
  h3 = TH1F("h3", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
  h4 = TH1F("h4", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
  h5 = TH1F("h5", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 
  h6 = TH1F("h6", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 

  for d in data_sample : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])

  c = TCanvas("c","", 900, 300)
  c.Divide(3, 1)
  h4.SetLineColor(2)
  h6.SetLineColor(2)
  c.cd(1); h1.Draw("zcol")
  c.cd(2); h3.Draw("e");
  c.cd(3); h5.Draw("e");
  c.Update()
