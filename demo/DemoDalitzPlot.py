# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf
import numpy as np
import array
import math
import os
from PIL import Image

from ROOT import TH1F, TH2F, TVirtualFitter, TCanvas, gStyle, gROOT, TColor, TImage, TLine, TArrow, TPad, TLatex, TEllipse

fptype = tf.float64

gROOT.ProcessLine(".x lhcbstyle2.C")

def HelicityAngle(m2ab, m2bc, md, ma, mb, mc) :
  md2 = md**2
  ma2 = ma**2
  mb2 = mb**2
  mc2 = mc**2
  m2ac = md2 + ma2 + mb2 + mc2 - m2ab - m2bc
  mab = tf.sqrt(m2ab)
  mac = tf.sqrt(m2ac)
  mbc = tf.sqrt(m2bc)
  p2a = 0.25/md2*(md2-(mbc+ma)**2)*(md2-(mbc-ma)**2)
  p2b = 0.25/md2*(md2-(mac+mb)**2)*(md2-(mac-mb)**2)
  p2c = 0.25/md2*(md2-(mab+mc)**2)*(md2-(mab-mc)**2)
  eb = (m2ab-ma2+mb2)/2./mab
  ec = (md2-m2ab-mc2)/2./mab
  pb = tf.sqrt(eb**2-mb2)
  pc = tf.sqrt(ec**2-mc2)
  e2sum = (eb+ec)**2
  m2bc_max = e2sum-(pb-pc)**2
  m2bc_min = e2sum-(pb+pc)**2
  return (m2bc_max + m2bc_min - 2.*m2bc)/(m2bc_max-m2bc_min)

class DalitzPhaseSpace : 

  def __init__(self, ma, mb, mc, md) : 
    self.ma = ma
    self.mb = mb
    self.mc = mc
    self.md = md
    self.ma2 = ma**2
    self.mb2 = mb**2
    self.mc2 = mc**2
    self.md2 = md**2
    self.msqsum = self.md2 + self.ma2 + self.mb2 + self.mc2
    self.minab = (ma + mb)**2
    self.maxab = (md - mc)**2
    self.minbc = (mb + mc)**2
    self.maxbc = (md - ma)**2

  def Inside(self, m2ab, m2bc) : 
    inside = tf.logical_and(tf.logical_and(tf.greater(m2ab, self.minab), tf.less(m2ab, self.maxab)), \
                            tf.logical_and(tf.greater(m2bc, self.minbc), tf.less(m2bc, self.maxbc)))
    eb = (m2ab - self.ma2 + self.mb2)/2./tf.sqrt(m2ab)
    ec = (self.md2 - m2ab - self.mc2)/2./tf.sqrt(m2ab)
    p2b = eb**2 - self.mb2
    p2c = ec**2 - self.mc2
    inside = tf.logical_and(inside, tf.logical_and(tf.greater(p2c, 0), tf.greater(p2b, 0)))
    m2bc_max = (eb+ec)**2 - (tf.sqrt(p2b) - tf.sqrt(p2c))**2
    m2bc_min = (eb+ec)**2 - (tf.sqrt(p2b) + tf.sqrt(p2c))**2
    return tf.logical_and(inside, tf.logical_and(tf.greater(m2bc, m2bc_min), tf.less(m2bc, m2bc_max) ) )

  def UniformSample(self, size, majorant = -1) : 
    v = [ tf.random_uniform([size], self.minab, self.maxab, dtype=fptype), 
          tf.random_uniform([size], self.minbc, self.maxbc, dtype=fptype) ] 
    if majorant>0 : v += [ tf.random_uniform([size], 0., majorant, dtype=fptype) ]
    dlz = tf.pack(v, axis=1)
    return tf.boolean_mask(dlz, self.Inside(v[0], v[1]) )

  def M2ab(self, sample) : return tf.transpose(sample)[0]

  def M2bc(self, sample) : return tf.transpose(sample)[1]

  def M2ac(self, sample) : return self.msqsum - self.M2ab(sample) - self.M2bc(sample)

  def HelicityAB(self, sample) : 
    return HelicityAngle(self.M2ab(sample), self.M2bc(sample), self.md, self.ma, self.mb, self.mc)

  def HelicityBC(self, sample) : 
    return HelicityAngle(self.M2bc(sample), self.M2ac(sample), self.md, self.mb, self.mc, self.ma)

  def HelicityAC(self, sample) : 
    return HelicityAngle(self.M2ac(sample), self.M2ab(sample), self.md, self.mc, self.ma, self.mb)

def Sum( ampls ) : return tf.add_n( ampls )

def Density(ampl) : return tf.complex_abs(ampl)

def Complex(re, im) : return tf.complex(re, im)

def Polar(a, ph) : return Complex(a*tf.cos(ph*3.14159265/180.), a*tf.sin(ph*3.14159265/180.))

def ConstPolar(a, ph) : return Complex(Const(a*math.cos(ph*3.14159265/180.)), Const(a*math.sin(ph*3.14159265/180.)))

def Const(c) : return tf.constant(c, dtype=fptype)

def RelativisticBreitWigner(m2ab, mres, wres) : 
  return 1./Complex(mres**2-m2ab, -mres*wres)

def HelicityAmplitude(x, spin) : 
  if spin == 0 : return Complex(Const(1.), Const(0.))
  if spin == 1 : return Complex(x, Const(0.))
  if spin == 2 : return Complex(x**2-1./3., Const(0.))
  if spin == 3 : return Complex(x**3-3./5.*x, Const(0.))
  return None

def ZemachTensor(m2ab, m2ac, m2bc, m2d, m2a, m2b, m2c, spin) : 
  if spin == 0 : return Complex( Const(1.), Const(0.))
  if spin == 1 : return Complex( m2ac-m2bc+(m2d-m2c)*(m2b-m2a)/m2ab, Const(0.))
  if spin == 2 : return Complex( (m2bc-m2ac+(m2d-m2c)*(m2b-m2a)/m2ab)**2-1./3.*(m2ab-2.*(m2d+m2c)+(m2d-m2c)**2/m2ab)*(m2ab-2.*(m2a+m2b)+(m2a-m2b)**2/m2ab), Const(0.))
  return None

if __name__ == "__main__" : 

  md = 1.865
  ma = 0.139
  mb = 0.497
  mc = 0.139

  m2d = md**2
  m2a = ma**2
  m2b = mb**2
  m2c = mc**2

  dlz = DalitzPhaseSpace(ma, mb, mc, md)

  def Resonance(m, w, spin, ch) : 
    if ch == 0 : 
      return RelativisticBreitWigner(dlz.M2ab(x), Const(m), Const(w))*ZemachTensor(dlz.M2ab(x), dlz.M2ac(x), dlz.M2bc(x), Const(m2d), Const(m2a), Const(m2b), Const(m2c), spin)
    if ch == 1 : 
      return RelativisticBreitWigner(dlz.M2bc(x), Const(m), Const(w))*ZemachTensor(dlz.M2bc(x), dlz.M2ab(x), dlz.M2ac(x), Const(m2d), Const(m2b), Const(m2c), Const(m2a), spin)
    if ch == 2 : 
      return RelativisticBreitWigner(dlz.M2ac(x), Const(m), Const(w))*ZemachTensor(dlz.M2ac(x), dlz.M2bc(x), dlz.M2ab(x), Const(m2d), Const(m2c), Const(m2a), Const(m2b), spin)
    return None

  sess = tf.Session()
  init = tf.initialize_all_variables()
  sess.run(init)

  w = 311
  h = 303

  px = 0.75
  py = 0.75

  cx = int(w*px)
  cy = int(h*py)

  h1 = TH2F("h1", "", w, dlz.minab-0.1*(dlz.maxab-dlz.minab) , dlz.maxab+0.1*(dlz.maxab-dlz.minab), h, dlz.minbc-0.1*(dlz.maxbc-dlz.minbc) , dlz.maxbc+0.1*(dlz.maxbc-dlz.minbc) )

  points = []
  ipoints = []
  for i in range(h1.GetNbinsX()) :
    for j in range(h1.GetNbinsY()) :
      x = h1.GetXaxis().GetBinCenter(i)
      y = h1.GetYaxis().GetBinCenter(j)
      points  += [ [x, y] ]
      ipoints += [ [i, j] ]
  x = Const(points)

  ampl  = Complex(Const(0.), Const(0.))
  ampl += ConstPolar(1.560805, 214.065284)*Resonance(0.522477, 0.453106, 0, 2)
  ampl += ConstPolar(1.      ,   0.      )*Resonance(0.775500, 0.149400, 1, 2)
  ampl += ConstPolar(0.491463,  64.390750)*Resonance(1.4590,   0.4550,   1, 2)
  ampl += ConstPolar(0.034337, 111.974402)*Resonance(0.782650, 0.008490, 1, 2)
  ampl += ConstPolar(0.385497, 207.278721)*Resonance(0.97700,  0.1,      0, 2)
  ampl += ConstPolar(0.203222, 212.128769)*Resonance(1.033172, 0.087984, 0, 2)
  ampl += ConstPolar(1.436933, 342.852060)*Resonance(1.275400, 0.185200, 2, 2)
  ampl += ConstPolar(1.561670, 109.586718)*Resonance(1.434000, 0.173000, 0, 2)

  ampl += ConstPolar(1.638345, 133.218073)*Resonance(0.891660, 0.050800, 1, 0)
  ampl += ConstPolar(0.651326, 119.929357)*Resonance(1.414000, 0.232000, 1, 0)
  ampl += ConstPolar(2.209476, 358.855281)*Resonance(1.414000, 0.290000, 0, 0)
  ampl += ConstPolar(0.890176, 314.789317)*Resonance(1.425600, 0.098500, 2, 0)
  ampl += ConstPolar(0.877231,  82.271754)*Resonance(1.717000, 0.322000, 1, 0)

  ampl += ConstPolar(0.149579, 325.356816)*Resonance(0.891660, 0.050800, 1, 1)
  ampl += ConstPolar(0.423211, 252.523919)*Resonance(1.414000, 0.232000, 1, 1)
  ampl += ConstPolar(0.364030,  87.118694)*Resonance(1.414000, 0.290000, 0, 1)
  ampl += ConstPolar(0.228236, 275.203595)*Resonance(1.425600, 0.098500, 2, 1)
  ampl += ConstPolar(2.081620, 130.047574)*Resonance(1.717000, 0.322000, 1, 1)

  ampl += ConstPolar(2.666864, 160.480162)

  inside = sess.run( dlz.Inside(dlz.M2ab(x), dlz.M2bc(x) ) )
  a = sess.run( ampl )

  imdata = np.zeros((h, w, 3), dtype=np.uint8)

  rmax = 0.
  for i in zip(ipoints, inside, a) : 
    c = complex(0., 0.)
    if i[1] : 
      c = i[2]
      h1.SetBinContent(i[0][0], i[0][1], 1)
    r = (c.real**2 + c.imag**2)**0.4
    if r>rmax : rmax = r

  for i in zip(ipoints, inside, a) : 
    x = i[0][0]
    y = i[0][1]
    if not i[1] : 
      imdata[h-y-1][x][0] = int(255.)
      imdata[h-y-1][x][1] = int(255.)
      imdata[h-y-1][x][2] = int(255.)
      continue
    c = i[2]
    r = (c.real**2 + c.imag**2)**0.4
    ph = math.atan2( c.imag, c.real )*180./3.14159265 + 180.
    ir = r/rmax*1.0
    if ir<0. : ir=0.
    if ir>=1. : ir = 1.
    iph = ph
    if iph<0. : iph = 0
    if iph>=360. : iph = 360.

    r = array.array('f', [0.])
    g = array.array('f', [0.])
    b = array.array('f', [0.])
    TColor.HLS2RGB(iph, ir, 1.-0.5*ir, r, g, b)
#    TColor.HLS2RGB(0., 1.-ir, 0, r, g, b)

    imdata[h-y-1][x][0] = int(0.+r[0]*255.)
    imdata[h-y-1][x][1] = int(0.+g[0]*255.)
    imdata[h-y-1][x][2] = int(0.+b[0]*255.)
#      print x, y, imdata[x][y]

  for i in range(-int(w/8), int(w/8)) : 
    for j in range(-int(h/8), int(h/8)) : 
      c = complex((8.*i)/w, (8.*j)/h)
      r = (c.real**2 + c.imag**2)**0.4
      ph = math.atan2( c.imag, c.real )*180./3.14159265 + 180.
      ir = r
      if ir<0. : ir=0.
      if ir>=1. : 
        continue
      iph = ph
      if iph<0. : iph = 0
      if iph>=360. : iph = 360.
      r = array.array('f', [0.])
      g = array.array('f', [0.])
      b = array.array('f', [0.])
      TColor.HLS2RGB(iph, ir, 1.-0.5*ir, r, g, b)
      imdata[h-cy-j][cx+i][0] = int(0.+r[0]*255.)
      imdata[h-cy-j][cx+i][1] = int(0.+g[0]*255.)
      imdata[h-cy-j][cx+i][2] = int(0.+b[0]*255.)

  img = Image.fromarray(imdata, 'RGB')
  img.save('tmp.png')
#  img.show()
  os.system("convert tmp.png tmp.eps")

  c = TCanvas("c","", 800, 800)
  h1.Draw("cont")
  h1.SetAxisColor(1, "xyz")
  h1.GetXaxis().SetTitle("m^{2}_{AB} [GeV^{2}]")
  h1.GetYaxis().SetTitle("m^{2}_{BC} [GeV^{2}]")
  img = TImage.Open("tmp.eps")
  img.Draw()
  c.RedrawAxis()

  a = TArrow()
  a.SetLineColor(1)
  a.SetLineWidth(2)
  ax = (1-px)*h1.GetXaxis().GetXmin()+px*h1.GetXaxis().GetXmax()
  ay = (1-py)*h1.GetYaxis().GetXmin()+py*h1.GetYaxis().GetXmax()
  lx = (h1.GetXaxis().GetXmax()-h1.GetXaxis().GetXmin())*0.15
  ly = (h1.GetYaxis().GetXmax()-h1.GetYaxis().GetXmin())*0.15

  a.DrawArrow(ax-lx, ay, ax+lx, ay, 0.02, "|>")
  a.DrawArrow(ax, ay-ly, ax, ay+ly, 0.02, "|>")

  t = TLatex()
  t.DrawLatex(ax+0.7*lx, ay+0.25*ly, "Re")
  t.DrawLatex(ax, ay+1.2*ly, "Im")

  e = TEllipse()
  e.SetLineColor(2)
  e.SetLineWidth(6)
  e.SetFillStyle(4000)
  e.DrawEllipse(ax, ay, lx/1.5, ly/1.5, 0., 360., 0.)

  h1.SetLineColor(2)
  h1.SetLineWidth(6)
  h1.SetContour(2)
  h1.SetContourLevel(2, 0.5)
  h1.Draw("cont3 same")

  c.Update()
  c.Print("DemoAmpl.eps")
  c.Print("DemoAmpl.png")
