import tensorflow as tf

import sys, os
sys.path.append("../")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from TensorFlowAnalysis import *

from ROOT import TFile

if __name__ == "__main__" : 

  mp  = 0.938
  mpi = 0.139
  mk  = 0.497
  mlb = 5.620

  phsp = FourBodyHelicityPhaseSpace(mp, mpi, mpi, mpi, mlb)
  uniform_sample = phsp.UniformSample(1000000)

  dd = Const(5.)
  dr = Const(1.5)

  mass_delta   = Const(1.232)
  width_delta  = Const(0.117)

  mass_nstar   = Const(1.440)
  width_nstar  = Const(0.350)

  ### Start of model description

  def model(x) : 
    (pa1, pa2, pb1, pb2) = phsp.FinalStateMomenta(x)

    # Final state momenta
    p_p    = pa1
    p_pim1 = pa2
    p_pip  = pb1
    p_pim2 = pb2

    # Phase space density (not uniform for 4-body decays)
    d = phsp.Density(x)

    # Resonance in ppipi (delta)

    p_ppipi = p_p+p_pim1+p_pip     # ppipi momentum
    m_ppipi = Mass(p_ppipi)        # ppipi invariant mass

    # Delta line shape. Assume it decays into N* and pi. 
    ampl1 = BreitWignerLineShape(m_ppipi**2, mass_delta,  width_delta, mp+mpi, mpi, mpi, mlb, dr, dd, 1, 1)

    # ppipi theta and phi angles in Lb rest frame
    (theta_ppipi, phi_ppipi) = SphericalAngles(p_ppipi)

    # Final state momenta in ppipi rest frame
    p1 = RotationAndBoost([pa1, pa2, pb1, pb2], p_ppipi)
    p1_p    = p1[0]
    p1_pim1 = p1[1]
    p1_pip  = p1[2]
    p1_pim2 = p1[3]

    p1_ppi = p1_p + p1_pim1  # ppi momentum in ppipi rest frame
    m_ppi = Mass(p1_ppi)     # ppi invariant mass

    # Nstar line shape.
    ampl2 = BreitWignerLineShape(m_ppi**2, mass_nstar,  width_nstar, mp, mpi, mpi, mass_delta, dr, dr, 1, 1)

    # ppi theta and phi angles in ppipi rest frame
    (theta_ppi, phi_ppi) = SphericalAngles(p1_ppi)

    # Proton spin rotation angles for ppi resonance
    p_ppi = p_p + p_pim1
    prot = RotationAndBoost([ p_ppi ], p_p)
    theta_rot_ppi, phi_rot_ppi = SphericalAngles( prot[0] )

    ampl = ampl1*ampl2

    pdf = Density(ampl)*d
    return pdf

  ### End of model description

  init = tf.global_variables_initializer()
  sess = tf.Session()
  sess.run(init)

  majorant = EstimateMaximum(sess, model(phsp.data_placeholder), phsp.data_placeholder, sess.run( phsp.UniformSample(1000000) ) )*1.1
  print "Maximum = ", majorant

  data = RunToyMC( sess, model(phsp.data_placeholder), phsp.data_placeholder, phsp, 1000000, majorant, chunk = 1000000)

  f = TFile.Open("toyresult.root", "RECREATE")
  FillNTuple("toy", data, ["m1", "m2", "cos1", "cos2", "phi" ] )
  f.Close()
