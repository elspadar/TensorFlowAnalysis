# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf

from timeit import default_timer as timer

import sys, os
sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT

from TensorFlowAnalysis import *
from tensorflow.python.client import timeline

phsp = RectangularPhaseSpace( ((-1., 1.), (-1., 1.)) )

SetSeed(1)
gmm = GaussianMixture2D("gmm", 10, (-1, 1), (-1, 1) )

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)

norm_sample = sess.run( phsp.RectangularGridSample( (200, 200) ) )
print "Normalisation sample size = ", len(norm_sample)
print norm_sample

def true_pdf(z) : 
  x = phsp.Coordinate(z, 0)
  y = phsp.Coordinate(z, 1)
  r = Sqrt(x**2 + y**2)
  return Exp(-0.5*(r-0.5)**2/0.1**2) + Exp(-0.5*r**2/0.1**2)

data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

true_model = true_pdf(data_ph)

data_sample = RunToyMC(sess, true_model, data_ph, phsp, 10000, 1. )

print data_sample
print gmm.params
print tf.trainable_variables()

c = TCanvas("c","c", 600, 300)
c.Divide(2,1)

h1 = TH2F("h1","",100, -1., 1., 100, -1., 1.)
h2 = TH2F("h2","",100, -1., 1., 100, -1., 1.)

for i in data_sample : h1.Fill(i[0], i[1])
c.cd(1)
h1.Draw("zcol")

options = None
run_metadata = None
#options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
#run_metadata = tf.RunMetadata()

n = 0
nmax = 1
data_model = gmm.model(data_ph)
norm_model = gmm.model(norm_ph)
nll = UnbinnedNLL( data_model, Integral( norm_model ) )

while(nmax <= len(gmm.params) ) : 

  if n>=0 : 
    gmm.fix()
    gmm.float(n)
  else : 
    for i in range(nmax) : gmm.float(i)

  result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, printout = 100, options = options, run_metadata = run_metadata )
  print result

  norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )

  h2.Reset()
  for i in zip(norm_sample, norm_pdf) : h2.Fill(i[0][0], i[0][1], i[1])

  c.cd(2)
  h2.Draw("zcol")

  c.Update()

  if n==-1 : 
    nmax += 1
    if nmax <= len(gmm.params): 
      print "Adding new component"
      n = nmax-1 
      gmm.params[n][0].init_value = 0.01
      gmm.params[n][0].fitted_value = 0.01
      gmm.params[n][0].update(sess, 0.01)
      gmm.params[n][1].randomise(sess, -0.5, 0.5)
      gmm.params[n][1].fitted_value = gmm.params[n][1].init_value
      gmm.params[n][2].randomise(sess, -0.5, 0.5)
      gmm.params[n][2].fitted_value = gmm.params[n][2].init_value
  else : 
    n -= 1

  if options : 
    fetched_timeline = timeline.Timeline(run_metadata.step_stats)
    chrome_trace = fetched_timeline.generate_chrome_trace_format()
    with open('timeline.json', 'w') as f:
      f.write(chrome_trace)
