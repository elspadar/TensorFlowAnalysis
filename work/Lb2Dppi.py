# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

#
# Example of three-body amplitude fit for baryonic decay Lambda_b^0 -> D0 p pi-
# The initial Lambda_b0 is assumed to be unpolarised, so only two degrees of 
# freedom remain, the fit is two-dimensional. 
#
# The amplitude formalism is taken from https://arxiv.org/abs/1701.07873
#

import tensorflow as tf

# Timer for benchmarking
from timeit import default_timer as timer

import sys, os
sys.path.append("../")

from ROOT import TH1F, TH2F, TCanvas, TFile

from TensorFlowAnalysis import *
from tensorflow.python.client import timeline

# Calculate orbital momentum for a decay of a particle 
# of a given spin and parity to a proton (J^P=1/2+) and a pseudoscalar. 
# All the spins and total momenta are expressed in units of 1/2
def OrbitalMomentum(spin, parity) : 
  l1 = (spin-1)/2     # Lowest possible momentum
  p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
  if p1 == parity : return l1
  return l1+1

# Return the sign in front of the complex coupling
# for amplitudes with baryonic intermediate resonances
# See Eq. (3), page 3 of LHCB-ANA-2015-072, 
# https://svnweb.cern.ch/cern/wsvn/lhcbdocs/Notes/ANA/2015/072/drafts/lb2dppi_aman_v3r4.pdf
def CouplingSign(spin, parity) : 
  jp =  1
  jd =  0
  pp =  1
  pd = -1
  s = 2*(((jp+jd-spin)/2+1) % 2)-1
  s *= (pp*pd*parity)
  return s

if __name__ == "__main__" : 

  # Default flags that can be overridden by command line options
  cache = False
  norm_grid = 400
  toy_sample = 10000
  gradient = True
  backend = "/gpu:0"
  profile = False

  # Fetch command line options
  for i in sys.argv[1:] :
    # Do not use GPU
    if i == "--nogpu" : 
      os.environ["CUDA_VISIBLE_DEVICES"] = ""
      backend = "/cpu:0"

    # Cache helicity amplitudes
    if i == "--cache" : 
      cache = True

    # Use 10^5 events data sample instead of default 10^4
    if i == "--100k" : 
      norm_grid = 800
      toy_sample = 100000

    # Do not use analytic gradient
    if i == "--nograd" : 
      gradient = False

    # Switch of TF profiling
    if i == "--profile" : 
      profile = True

  # Masses of initial and final state particles
  mlb = 5.620
  md  = 1.865
  mpi = 0.140
  mp  = 0.938

  with tf.device(backend) : 

    # Create phase space object for 3-body baryonic decay
    # Use only a subrange of D0p invariant masses
    phsp = Baryonic3BodyPhaseSpace(md, mp, mpi, mlb, mabrange = (0., 3.) )

    # Constant parameters of intermediate resonances
    mass_lcst   = Const(2.88153)
    width_lcst  = Const(0.0058)

    mass_lcx    = Const(2.857)
    width_lcx   = Const(0.060)

    mass_lcstst   = Const(2.945)
    width_lcstst  = Const(0.026)

    mass0 = Const(3.)

    # Blatt-Weisskopf radii
    db = Const(5.)
    dr = Const(1.5)

    # Slope parameters for exponential nonresonant amplitudes
    alpha12p = FitParameter("alpha12p", 2.3, 0., 10., 0.01)
    alpha12m = FitParameter("alpha12m", 1.0, 0., 10., 0.01)
    alpha32p = FitParameter("alpha32p", 2.5, 0., 10., 0.01)
    alpha32m = FitParameter("alpha32m", 2.6, 0., 10., 0.01)

    # List of complex couplings
    couplings = [
    (
        Complex(Const(1.), Const(0.) ), 
        Complex(Const(0.), Const(0.) ) 
    ), 
    (
        Complex(FitParameter("ArX1", -0.38, -10., 10., 0.01), FitParameter("AiX1",  0.86, -10., 10., 0.01) ), 
        Complex(FitParameter("ArX2",  6.59, -10., 10., 0.01), FitParameter("AiX2", -0.38, -10., 10., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar29401",  0.53, -10., 10., 0.01), FitParameter("Ai29401", 0.14, -10., 10., 0.01) ), 
        Complex(FitParameter("Ar29402", -1.24, -10., 10., 0.01), FitParameter("Ai29402", 0.02, -10., 10., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar12p1",  0.05, -10., 10., 0.01), FitParameter("Ai12p1",  0.23, -10., 10., 0.01) ), 
        Complex(FitParameter("Ar12p2", -0.16, -10., 10., 0.01), FitParameter("Ai12p2", -2.86, -10., 10., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar12m1",  1.17, -10., 10., 0.01), FitParameter("Ai12m1", 0.76, -10., 10., 0.01) ), 
        Complex(FitParameter("Ar12m2", -2.55, -10., 10., 0.01), FitParameter("Ai12m2", 3.86, -10., 10., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar32p1",  0., -100., 100., 0.01), FitParameter("Ai32p1",  0., -100., 100., 0.01) ), 
        Complex(FitParameter("Ar32p2",  0., -100., 100., 0.01), FitParameter("Ai32p2",  0., -100., 100., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar32m1",  0.95, -10., 10., 0.01), FitParameter("Ai32m1", -0.45, -10., 10., 0.01) ), 
        Complex(FitParameter("Ar32m2", -2.27, -10., 10., 0.01), FitParameter("Ai32m2",  0.95, -10., 10., 0.01) )
    )
    ]

  # Switches (boolean flags) allow switching on-off of various amplitude components.
  # Used for calculation of fit fractions or producing component weights in fitted distributions for control plots. 
  switches = Switches(len(couplings))

  with tf.device(backend) : 
#  with tf.device("/gpu:0") : 

    # Model description
    def model(x) : 

      m2dp  = phsp.M2ab(x)
      m2ppi = phsp.M2bc(x)

      p4d, p4p, p4pi = phsp.FinalStateMomenta(m2dp, m2ppi, 0., 0., 0.)
      dp_theta_r, dp_phi_r, dp_theta_d, dp_phi_d = HelicityAngles3Body(p4d, p4p, p4pi)

      # List of intermediate resonances corresponds to arXiv link above. 
      resonances = [
      ( BreitWignerLineShape(m2dp, mass_lcst, width_lcst, md, mp, mpi, mlb, dr, db, OrbitalMomentum(5, 1), 2), 5, 1, 
        couplings[0][0], couplings[0][1]
      ), 
      ( BreitWignerLineShape(m2dp, mass_lcx, width_lcx, md, mp, mpi, mlb, dr, db, OrbitalMomentum(3, 1), 1), 3, 1, 
        couplings[1][0], couplings[1][1]
      ), 
      ( BreitWignerLineShape(m2dp, mass_lcstst, width_lcstst, md, mp, mpi, mlb, dr, db, OrbitalMomentum(3, -1), 1), 3, -1, 
        couplings[2][0], couplings[2][1]
      ), 
      ( ExponentialNonResonantLineShape(m2dp, mass0, alpha12p, md, mp, mpi, mlb, OrbitalMomentum(1, 1), 0), 1, 1, 
        couplings[3][0], couplings[3][1]
      ), 
      ( ExponentialNonResonantLineShape(m2dp, mass0, alpha12m, md, mp, mpi, mlb, OrbitalMomentum(1,-1), 0), 1,-1, 
        couplings[4][0], couplings[4][1]
      ),
      ( ExponentialNonResonantLineShape(m2dp, mass0, alpha32p, md, mp, mpi, mlb, OrbitalMomentum(3, 1), 1), 3, 1, 
        couplings[5][0], couplings[5][1]
      ), 
      ( ExponentialNonResonantLineShape(m2dp, mass0, alpha32m, md, mp, mpi, mlb, OrbitalMomentum(3,-1), 1), 3,-1, 
        couplings[6][0], couplings[6][1]
      ), 
      ]

      density = Const(0.)

      # Decay density is an incoherent sum over initial and final state polarisations 
      # (assumong no polarisation for Lambda_b^0), and for each polarisation combination 
      # it is a coherent sum over intermediate states (including two polarisations of 
      # the intermediate resonance). 
      for pol_lb in [-1, 1] : 
        for pol_p in [-1, 1] : 
          ampl = Complex(Const(0.), Const(0.))
          for r,s in zip(resonances, switches) : 
            lineshape = r[0]
            spin = r[1]
            parity = r[2]
            if pol_p == -1 : 
              sign = CouplingSign(spin, parity)
              coupling1 = r[3]*sign
              coupling2 = r[4]*sign
            else : 
              coupling1 = r[3]
              coupling2 = r[4]
            ampl += Complex(s, Const(0.))*coupling1*lineshape*\
                  HelicityAmplitude3Body(dp_theta_r, dp_phi_r, dp_theta_d, dp_phi_d, 1, spin, pol_lb,  1, 0, pol_p, 0, cache = cache)
            ampl += Complex(s, Const(0.))*coupling2*lineshape*\
                  HelicityAmplitude3Body(dp_theta_r, dp_phi_r, dp_theta_d, dp_phi_d, 1, spin, pol_lb, -1, 0, pol_p, 0, cache = cache)
          density += Density(ampl)

      return density

  with tf.device(backend) : 
#  with tf.device("/gpu:0") : 

    # Data and normalisation sample placeholders
    data_ph = phsp.data_placeholder
    norm_ph = phsp.norm_placeholder

    # TF graphs for data and normalisation that will enter unbinned likelihood
    data_pdf = model(data_ph)
    norm_pdf = model(norm_ph)

  # Open TF session
  SetSeed(2)
  sess = tf.Session()

  # TF initialisation
  init = tf.global_variables_initializer()
  sess.run(init)

  # Produce normalisation sample (rectangular 2D grid of points)
  norm_sample = sess.run( phsp.RectangularGridSample(norm_grid, norm_grid) )
  print "Normalisation sample size = ", len(norm_sample)

  # Calculate maximum of the PDF for accept-reject toy MC generation
  majorant = EstimateMaximum(sess, data_pdf, data_ph, norm_sample )*1.5
  print "Maximum = ", majorant

  # Create toy MC data sample
  data_sample = RunToyMC( sess, data_pdf, data_ph, phsp, toy_sample, majorant, chunk = 1000000)

  # Store it in ROOT NTuple
  f = TFile.Open("toy.root", "RECREATE")
  FillNTuple("toy", data_sample, ["m2dp", "m2ppi" ] )
  f.Close()

  # Calculate fit fractions for individual components and store then in a text file
  ff = CalculateFitFractions(sess, data_pdf, data_ph, switches, norm_sample = norm_sample )
  WriteFitFractions(ff, ["Lc2880", "LcX", "Lc2940", "NR12p", "NR12m", "NR32p", "NR32m"], "fitfractions_init.txt")

  # Decide on profiling options if needed
  if profile : 
    options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    run_metadata = tf.RunMetadata()
  else : 
    options = None
    run_metadata = None

  # Perform unbinned neg. log likelihood fit to the generated sample
  with tf.device(backend) : 
    nll = UnbinnedNLL( data_pdf, Integral( norm_pdf) )
    start = timer()
    result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = gradient,  
                       options = options, run_metadata = run_metadata )
    end = timer()
    print(end - start) 

  # Store fit result in a text file
  print result
  WriteFitResults(result, "result.txt")

  # Calculate fit fractions for the fitted amplitude
  ff = CalculateFitFractions(sess, data_pdf, data_ph, switches, norm_sample = norm_sample)
  WriteFitFractions(ff, ["Lc2880", "LcX", "Lc2940", "NR12p", "NR12m", "NR32p", "NR32m"], "fitfractions.txt")

  # Create toy MC sample corresponding to the fitted distribution with weights for individual components
  # (the latter is controlled by providing "switches = ..." argument) and store it in a ROOT NTuple
  fit_sample = RunToyMC(sess, data_pdf, data_ph, phsp, 100000, majorant, chunk = 1000000, switches = switches)
  f = TFile.Open("toyresult.root", "RECREATE")
  FillNTuple("toy", fit_sample, ["m2dp", "m2ppi" ] + [ "w%d" % (n+1) for n in range(len(switches)) ])
  f.Close()

  # Plot generated distribution and fit result
  h1 = TH2F("h1", "", 100, 2.8, 3.0, 100, 4., 13. ) 
  h2 = TH2F("h2", "", 100, 2.8, 3.0, 100, 4., 13. ) 
  h3 = TH1F("h3", "", 100, 2.8, 3.0 ) 
  h4 = TH1F("h4", "", 100, 2.8, 3.0 ) 
  h5 = TH1F("h5", "", 100, 4., 13. ) 
  h6 = TH1F("h6", "", 100, 4., 13. ) 

  for d in data_sample : 
    h1.Fill(math.sqrt(d[0]), d[1])
    h3.Fill(math.sqrt(d[0]) )
    h5.Fill(d[1])

  for f in fit_sample : 
    h2.Fill(math.sqrt(f[0]), f[1])
    h4.Fill(math.sqrt(f[0]) )
    h6.Fill(f[1])

  c = TCanvas("c","", 600, 600)
  c.Divide(2, 2)
  h4.SetLineColor(2)
  h6.SetLineColor(2)
  c.cd(1); h1.Draw("zcol")
  c.cd(2); h2.Draw("zcol")
  c.cd(3); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
  c.cd(4); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
  c.Update()

  print(end - start) 

  # Store benchmarking information
  f = open("timing.txt","a")
  f.write(str(sys.argv) + " : %f sec, %d iterations\n" % (end-start, result["iterations"]))
  f.close()

  if profile : 
    # Store profiling information
    fetched_timeline = timeline.Timeline(run_metadata.step_stats)
    chrome_trace = fetched_timeline.generate_chrome_trace_format()
    with open('timeline.json', 'w') as f:
      f.write(chrome_trace)
