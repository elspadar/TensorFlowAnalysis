# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

#
# Example of three-body amplitude fit for baryonic decay Lambda_c^+ -> p K- pi+
# The initial Lambda_c^+ is assumed to be unpolarised, so only two degrees of 
# freedom remain, the fit is two-dimensional. 
#
# The fit model includes realistic treatment of non-uniform acceptance (defined by the 
# 2D histogram in square Dalitz plot coordimates) and background (assumed to be constant 
# over phase space)
#
# See, e.g. https://arxiv.org/abs/1701.07873 for the description of the amplitude 
# formalism for three-body baryonic decays. 
#

import tensorflow as tf

from timeit import default_timer as timer
import sys, os

# Add path to TensorFlowAnalysis functions
sys.path.append("../")
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU

from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT

from TensorFlowAnalysis import *

# Masses of initial and final state particles
mLc = 2.286
mp  = 0.938
mK  = 0.497
mPi = 0.140

# Blatt-Weisskopf radii
rLc = Const(5.) # For Lc
rR  = Const(1.5) # For all intermediate resonances

# Flag to control caching of helicity tensors
cache = True

# Calculate orbital momentum for a decay of a particle 
# of a given spin and parity to a proton (J^P=1/2+) and a pseudoscalar. 
# All the spins and total momenta are expressed in units of 1/2
def BaryonDecayOrbitalMomentum(spin, parity) : 
  l1 = (spin-1)/2     # Lowest possible momentum
  p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
  if p1 == parity : return l1
  return l1+1

# Return the sign in front of the complex coupling
# for amplitudes with baryonic intermediate resonances
# See Eq. (3), page 3 of LHCB-ANA-2015-072, 
# https://svnweb.cern.ch/cern/wsvn/lhcbdocs/Notes/ANA/2015/072/drafts/lb2dppi_aman_v3r4.pdf

def BaryonCouplingSign(spin, parity) : 
  jp =  1
  jd =  0
  pp =  1
  pd = -1
  s = 2*(((jp+jd-spin)/2+1) % 2)-1
  s *= (pp*pd*parity)
  return s

# Dictionary to describe the amplitude structure
# The dictionary has the structure 
#   "Resonance name" : Resonance
# where Resonance is again a dictionary describing a single resonance
# with the following properties: 
#   "channel"   : resonance channel 
#                     (0 for ppi resonance, 
#                      1 for Kpi resonance, 
#                      2 for Kp resonance)
#   "mass"      : Mass of the resonance 
#   "width"     : Width of the resonance. 
#                 Mass and width should be TF graphs, e.g. Const(...), FitParameter(...) etc.
#   "spin"      : spin of the resonance (in units of 1/2, i.e. 1 for spinor, 2 for vector etc.)
#   "parity"    : parity (1 or -1)
#   "couplings" : list of complex couplings. Two for baryons and four for meson resonances
#   "lineshape" : optional line shape class. By default, BreitWignerLineShape is used. 
resonances = {
  "Kstar" : {
    "channel"   : 1,            
    "mass"      : Const(0.892), 
    "width"     : Const(0.051), 
    "spin"      : 2, 
    "parity"    : -1, 
    "couplings" : [              
        Complex(FitParameter("ArKst_1",  1., -20., 20., 0.01), FitParameter("AiKst_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArKst_2",  0., -20., 20., 0.01), FitParameter("AiKst_2",  0.0, -20., 20., 0.01) ),
        Complex(FitParameter("ArKst_3",  0., -20., 20., 0.01), FitParameter("AiKst_3",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArKst_4",  0., -20., 20., 0.01), FitParameter("AiKst_4",  0.0, -20., 20., 0.01) ),
                  ]
  }, 
  "L1405" : {
    "channel"   : 2, 
    "lineshape" : SubThresholdBreitWignerLineShape, 
    "mass"      : Const(1.405), 
    "width"     : Const(0.050), 
    "spin"      : 1, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1405_1",  3., -20., 20., 0.01), FitParameter("AiL1405_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1405_2",  0., -20., 20., 0.01), FitParameter("AiL1405_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "L1520" : {
    "channel"   : 2, 
    "mass"      : FitParameter("M1520", 1.5195, 1.50, 1.55, 0.0001), 
    "width"     : FitParameter("G1520", 0.0156, 0.01, 0.02, 0.0001), 
    "spin"      : 3, 
    "parity"    : -1, 
    "couplings" : [
        Complex(Const(1.), Const(0.) ), 
        Complex(Const(0.), Const(0.) ),
                  ]
  }, 
  "L1600" : {
    "channel"   : 2, 
    "mass"      : Const(1.600), 
    "width"     : Const(0.150), 
    "spin"      : 1, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArL1600_1",  1., -20., 20., 0.01), FitParameter("AiL1600_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1600_2",  0., -20., 20., 0.01), FitParameter("AiL1600_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "L1670" : {
    "channel"   : 2, 
    "mass"      : FitParameter("M1670", 1.670, 1.600, 1.700, 0.0001), 
    "width"     : FitParameter("G1670", 0.035, 0.020, 0.045, 0.0001), 
    "spin"      : 1, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1670_1",  1., -20., 20., 0.01), FitParameter("AiL1670_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1670_2",  0., -20., 20., 0.01), FitParameter("AiL1670_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "L1690" : {
    "channel"   : 2, 
    "mass"      : Const(1.690), 
    "width"     : Const(0.060), 
    "spin"      : 3, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1690_1",  1., -20., 20., 0.01), FitParameter("AiL1690_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1690_2",  0., -20., 20., 0.01), FitParameter("AiL1690_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "L1800" : {
    "channel"   : 2, 
    "mass"      : Const(1.800), 
    "width"     : Const(0.300), 
    "spin"      : 1, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1800_1",  1., -20., 20., 0.01), FitParameter("AiL1800_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1800_2",  0., -20., 20., 0.01), FitParameter("AiL1800_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "L1810" : {
    "channel"   : 2, 
    "mass"      : Const(1.810), 
    "width"     : Const(0.150), 
    "spin"      : 1, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArL1810_1",  1., -20., 20., 0.01), FitParameter("AiL1810_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1810_2",  0., -20., 20., 0.01), FitParameter("AiL1810_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "L1820" : {
    "channel"   : 2, 
    "mass"      : Const(1.820), 
    "width"     : Const(0.080), 
    "spin"      : 5, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArL1820_1",  1., -20., 20., 0.01), FitParameter("AiL1820_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1820_2",  0., -20., 20., 0.01), FitParameter("AiL1820_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "L1830" : {
    "channel"   : 2, 
    "mass"      : Const(1.830), 
    "width"     : Const(0.095), 
    "spin"      : 5, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1830_1",  1., -20., 20., 0.01), FitParameter("AiL1830_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1830_2",  0., -20., 20., 0.01), FitParameter("AiL1830_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "L1890" : {
    "channel"   : 2, 
    "mass"      : Const(1.890), 
    "width"     : Const(0.100), 
    "spin"      : 3, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArL1890_1",  1., -20., 20., 0.01), FitParameter("AiL1890_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArL1890_2",  0., -20., 20., 0.01), FitParameter("AiL1890_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "D1232" : {
    "channel"   : 0, 
    "mass"      : Const(1.232), 
    "width"     : Const(0.100), 
    "spin"      : 3, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArD1232_1",  5., -20., 20., 0.01), FitParameter("AiD1232_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArD1232_2",  0., -20., 20., 0.01), FitParameter("AiD1232_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "D1600" : {
    "channel"   : 0, 
    "mass"      : Const(1.600), 
    "width"     : Const(0.275), 
    "spin"      : 3, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArD1600_1",  0.1, -20., 20., 0.01), FitParameter("AiD1600_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArD1600_2",  0., -20., 20., 0.01), FitParameter("AiD1600_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "D1620" : {
    "channel"   : 0, 
    "mass"      : Const(1.620), 
    "width"     : Const(0.130), 
    "spin"      : 1, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArD1620_1",  1., -20., 20., 0.01), FitParameter("AiD1620_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArD1620_2",  0., -20., 20., 0.01), FitParameter("AiD1620_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
  "D1700" : {
    "channel"   : 0, 
    "mass"      : Const(1.650), 
    "width"     : Const(0.230), 
    "spin"      : 3, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArD1700_1",  0.1, -20., 20., 0.01), FitParameter("AiD1700_1",  0.0, -20., 20., 0.01) ), 
        Complex(FitParameter("ArD1700_2",  0. , -20., 20., 0.01), FitParameter("AiD1700_2",  0.0, -20., 20., 0.01) )
                  ]
  }, 
}

# Return the list of resonance names from the dictionary above
def listComponentNames(res) : 
  return sorted(res.keys())

# Return the number of resonance components in the dictionary above
def numberOfComponents(res) : 
  return len(listComponentNames(res))

# Return the dictionary of amplitude components for a single resonance contribution
# as a dictionary key:value, where
#   key   : a tuple (mu, lambda_p)  (mu is a Lc spin projection, lambda_p is a proton helicity)
#   value : TF graph for the calculation of the corresponding amplitude for a single resonance
# Input parameters: 
#   res : a dictionary representing a single resonance (as in the "resonances" dictionary above)
#   var : dictionary of kinematic variables characterising a particular candidate
#   sw  : list of boolean "switches" to turn components on/off for fit fraction calculation
def getHelicityAmplitudes(res, var, sw) : 
  channel = res["channel"]

  # Determine lineshape function
  if "lineshape" in res : 
    lineShapeFunc = res["lineshape"]
  else : 
    lineShapeFunc = BreitWignerLineShape

  # Create the TF graph for lineshape depending on appropriate 
  # free parameters and kinematic variables
  if channel == 1 : 
    lineshape = lineShapeFunc(var["m2kpi"], res["mass"], res["width"], 
                          mK, mPi, mp, mLc, rR, rLc, res["spin"]/2, res["spin"]/2-1)
  if channel == 2 : 
    lineshape = lineShapeFunc(var["m2pk"], res["mass"], res["width"], 
                          mp, mK, mPi, mLc, rR, rLc, BaryonDecayOrbitalMomentum(res["spin"], res["parity"]), (res["spin"]-1)/2)
  if channel == 0 : 
    lineshape = lineShapeFunc(var["m2ppi"], res["mass"], res["width"], 
                          mp, mPi, mK, mLc, rR, rLc, BaryonDecayOrbitalMomentum(res["spin"], res["parity"]), (res["spin"]-1)/2)

  ampl = {}

  # Loop over initial and final state polarisations
  # (amplitudes for them will be stored separately, and then added up incoherently)
  # For baryonic resonances, this follows Eq. (6), page 5 of the LHCb-ANA-2015-072, 
  # https://svnweb.cern.ch/cern/wsvn/lhcbdocs/Notes/ANA/2015/072/drafts/lb2dppi_aman_v3r4.pdf
  for pol_l in [-1, 1] : 
    for pol_p in [-1, 1] : 
      # Choose couplings depending on channel and polarisation
      if channel == 1 : # Kpi resonance
        if pol_p == -1 : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
          ampl[ (pol_l, pol_p) ] = coupling1*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  0, 0, 0, pol_p, cache = cache) + \
                                   coupling2*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l, -2, 0, 0, pol_p, cache = cache)
        else : 
          coupling1 = res["couplings"][2]
          coupling2 = res["couplings"][3]
          ampl[ (pol_l, pol_p) ] = coupling1*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  2, 0, 0, pol_p, cache = cache) + \
                                   coupling2*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  0, 0, 0, pol_p, cache = cache)

      if channel == 0 : # Delta++ resonance
        if pol_p == -1 : 
          sign = BaryonCouplingSign(res["spin"], res["parity"])
          coupling1 = res["couplings"][0]*sign
          coupling2 = res["couplings"][1]*sign
        else : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
        ampl[ (pol_l, pol_p) ] = sw*coupling1*lineshape*\
                                    HelicityAmplitude3Body(var["pk_theta_r"], var["pk_phi_r"], var["pk_theta_p"], var["pk_phi_p"], 
                                    1, res["spin"], pol_l,  1, 0, pol_p, 0, cache = cache) + \
                                 sw*coupling2*lineshape*\
                                    HelicityAmplitude3Body(var["pk_theta_r"], var["pk_phi_r"], var["pk_theta_p"], var["pk_phi_p"], 
                                    1, res["spin"], pol_l, -1, 0, pol_p, 0, cache = cache)

      if channel == 2 : # Lambda* resonance
        if pol_p == -1 : 
          sign = BaryonCouplingSign(res["spin"], res["parity"])
          coupling1 = res["couplings"][0]*sign
          coupling2 = res["couplings"][1]*sign
        else : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
        ampl[ (pol_l, pol_p) ] = sw*coupling1*lineshape*\
                                    HelicityAmplitude3Body(var["ppi_theta_r"], var["ppi_phi_r"], var["ppi_theta_p"], var["ppi_phi_p"], 
                                    1, res["spin"], pol_l,  1, 0, pol_p, 0, cache = cache) + \
                                 sw*coupling2*lineshape*\
                                    HelicityAmplitude3Body(var["ppi_theta_r"], var["ppi_phi_r"], var["ppi_theta_p"], var["ppi_phi_p"], 
                                    1, res["spin"], pol_l, -1, 0, pol_p, 0, cache = cache)

  # Perform rotation of proton spin quantisation axis depending on channel
  rot_angle = SpinRotationAngle( var["p4p"], var["p4k"], var["p4pi"], channel)
  c = Complex( Cos(rot_angle/2.), Const(0.) )
  s = Complex( Sin(rot_angle/2.), Const(0.) )
  for pol_l in [-1, 1] : 
    a1 = ampl[ (pol_l, -1) ]
    a2 = ampl[ (pol_l,  1) ]
    ( ampl[ (pol_l, -1) ], ampl[ (pol_l, 1) ]) = ( c*a1 - s*a2, s*a1 + c*a2 )

  return ampl


# Create the phase space object
phsp = Baryonic3BodyPhaseSpace(mp, mK, mPi, mLc )

# Read acceptance histogram
f = TFile.Open("Lc2pKpiAcceptance.root")
effhist = f.Get("m13vst13CFtrackEff")
effhist.SetDirectory(0)
f.Close()

# Create the TF graph for acceptance from the ROOT histogram
effhist.Smooth()
effshape = RootHistShape(effhist)

# Create the list of boolean switches for amplitude components
#  "+1" for an additional switch corresponding to background component
switches = Switches(numberOfComponents(resonances)+1)

# Background magnitude is a free parameter
bck = FitParameter("B",  10., 0., 1000., 0.01)

# Function that returns the TF graph for a decay density for a phase space tensor "x"
def model(x) : 

    # Calculate invariant masses at the phasespace point "x"
    m2pk  = phsp.M2ab(x)
    m2ppi = phsp.M2ac(x)
    m2kpi = phsp.M2bc(x)

    # Final state 4-momenta for a three-body decay
    # Since we assume that Lc is unpolarised, can take any 
    # values of the 3-bpdy plane rotation angles. 
    # The density will not depend on them. Set them all equal to zero. 
    p4p, p4k, p4pi = phsp.FinalStateMomenta(m2pk, m2kpi, 0.0, 0.0, 0.0)

    # Helicity angles for each of the three two-body decay channels
    #  theta_r and phi_r are the polar and azimuthal angles of Lc->RC decay
    #  theta_b and phi_b are the polar and azimuthal angles of the R->AB decay
    kpi_theta_r, kpi_phi_r, kpi_theta_k, kpi_phi_k = HelicityAngles3Body(p4k, p4pi, p4p)
    pk_theta_r, pk_phi_r, pk_theta_p, pk_phi_p     = HelicityAngles3Body(p4p, p4k, p4pi)
    ppi_theta_r, ppi_phi_r, ppi_theta_p, ppi_phi_p = HelicityAngles3Body(p4p, p4pi, p4k)

    # Dictionary of all kinematic variables characterising a candidate
    var = {
      "m2pk" : m2pk, 
      "m2kpi" : m2kpi, 
      "m2ppi" : m2ppi, 
      "p4p" : p4p, 
      "p4k" : p4k, 
      "p4pi" : p4pi, 
      "kpi_theta_r" : kpi_theta_r, 
      "kpi_phi_r"   : kpi_phi_r, 
      "kpi_theta_k" : kpi_theta_k, 
      "kpi_phi_k"   : kpi_phi_k, 
      "pk_theta_r"  : pk_theta_r, 
      "pk_phi_r"    : pk_phi_r, 
      "pk_theta_p"  : pk_theta_p, 
      "pk_phi_p"    : pk_phi_p, 
      "ppi_theta_r" : ppi_theta_r, 
      "ppi_phi_r"   : ppi_phi_r, 
      "ppi_theta_p" : ppi_theta_p, 
      "ppi_phi_p"   : ppi_phi_p, 
    }

    ampls = {}

    # Start with zero amplitude
    for pol_l in [-1, 1] : 
      for pol_p in [-1, 1] : 
        ampls[ (pol_l, pol_p) ] = Complex(Const(0.), Const(0.))

    # Loop over all resonances, add up amplitudes corresponding to 
    # initial and final state polarisations coherently
    for i,n in enumerate(listComponentNames(resonances)) : 
      res = resonances[n]
      sw = Complex(switches[i], Const(0.))
      a = getHelicityAmplitudes(res, var, sw)
      for pol_l in [-1, 1] : 
        for pol_p in [-1, 1] : 
          ampls[ (pol_l, pol_p) ] += a[ (pol_l, pol_p) ]

    # Decay density is an incoherent sum of |a|^2 for each 
    # initial and final state polarisation 
    density = Const(0.)
    for pol_l in [-1, 1] : 
      for pol_p in [-1, 1] : 
        density += Density( ampls[ (pol_l, pol_p) ] )

    # Add acceptance and background
    # Acceptance is defined for square Dalitz plot, so use MPrime and ThetaPrime variables
    return density*effshape.shape( tf.stack([phsp.ThetaPrimeAC(x), phsp.MPrimeAC(x)], axis = 1) )*1e3 + bck*switches[-1]

# Placeholders for data and normalisation samples
data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

# TF graphs for decay density as functions of data and normalisation placeholders
data_model = model(data_ph)
norm_model = model(norm_ph)

# Set random seed and create TF session
SetSeed(1)
sess = tf.Session()

# Initialise TF
init = tf.global_variables_initializer()
sess.run(init)

# Create normalisation sample (1000x1000 2D grid over the phase space)
norm_sample = sess.run( phsp.RectangularGridSample(1000, 1000) )
print "Normalisation sample size = ", len(norm_sample)

#  majorant = EstimateMaximum(sess, data_model, data_ph, norm_sample )*1.5
#  print "Maximum = ", majorant
#  data_sample = RunToyMC( sess, data_model, data_ph, phsp, 500000, majorant, chunk = 1000000)

#  f = TFile.Open("toy.root", "RECREATE")
#  FillNTuple("toy", data_sample, ["m2pk", "m2kpi" ])
#  f.Close()

# Open the data ntuple
f = TFile.Open("lc2pKpi.root")
nt = f.Get("nt")
data_sample1 = ReadNTuple(nt, [ "m2pk", "m2kpi" ] ) 
data_sample = sess.run(phsp.Filter(data_sample1) )

# Create the TF graph for unbinned NLL from the data and normlisation graphs
nll = UnbinnedNLL( data_model, Integral( norm_model ) )

# Read starting values of fit parameters 
ReadFitResults(sess, "result.txt")

# Run minimisation and store results
start = timer()
result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = True )
end = timer()
print(end - start) 
print result
WriteFitResults(result, "result.txt")

f.Close()

# Calculate and store fit fractions
ff = CalculateFitFractions(sess, data_model, data_ph, switches, norm_sample = norm_sample)
WriteFitFractions(ff, listComponentNames(resonances) + ["Bkgr"], "fitfractions.txt")

# Create toy MC sample corresponding to the fit result
majorant = EstimateMaximum(sess, data_model, data_ph, norm_sample )*1.5
fit_sample = RunToyMC(sess, data_model, data_ph, phsp, 2000000, majorant, chunk = 1000000, switches = switches )
f = TFile.Open("toyresult.root", "RECREATE")
FillNTuple("toy", fit_sample, ["m2pk", "m2kpi" ] + [ "w%d" % (n+1) for n in range(len(switches)) ] )
f.Close()

# Plot fit results
h1 = TH2F("h1", "", 200, phsp.minab-0.2, phsp.maxab+0.2, 200, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
h2 = TH2F("h2", "", 200, phsp.minab-0.2, phsp.maxab+0.2, 200, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 

h3 = TH1F("h3", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
h4 = TH1F("h4", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
h5 = TH1F("h5", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 
h6 = TH1F("h6", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 
h7 = TH1F("h7", "", 100, phsp.minac-0.2, phsp.maxac+0.2 ) 
h8 = TH1F("h8", "", 100, phsp.minac-0.2, phsp.maxac+0.2 ) 

for d in data_sample : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])

for d in sess.run(phsp.M2ac(data_sample)) : 
    h7.Fill(d)

for f in fit_sample : 
    h2.Fill(f[0], f[1])
    h4.Fill(f[0])
    h6.Fill(f[1])

for f in sess.run(phsp.M2ac(fit_sample)) : 
    h8.Fill(f)

gROOT.ProcessLine(".x lhcbstyle2.C")

c = TCanvas("c","", 900, 600)
c.Divide(3, 2)
gStyle.SetPalette(107)
h4.SetLineColor(2)
h6.SetLineColor(2)
h8.SetLineColor(2)
h1.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h1.GetYaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h2.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h2.GetYaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h3.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h5.GetXaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h7.GetXaxis().SetTitle("M^{2}(p#pi) [GeV^{2}]")

c.cd(1); h1.Draw("zcol")
c.cd(2); h2.Draw("zcol")
c.cd(4); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
c.cd(5); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
c.cd(6); h7.Draw("e"); h8.Scale(h7.Integral()/h8.Integral()); h8.Draw("h same")
c.Update()
c.Print("Lc2pKpi.pdf")

print(end - start) 
