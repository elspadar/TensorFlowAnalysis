# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf
import numpy as np
import math

import sys, os
sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

from Interface import *
from Kinematics import *
from DalitzPhaseSpace import *

class Baryonic3BodyPhaseSpace(DalitzPhaseSpace) :
  """
    Derived class for baryonic 3-body decay, baryon -> scalar scalar baryon
  """
  def FinalStateMomenta(self, m2ab, m2bc, thetab, phib, phiac) :
    """
      Calculate 4-momenta of final state tracks in the 5D phase space
        m2ab, m2bc : invariant masses of AB and BC combinations
        thetab, phib : direction angles of the particle B in the reference frame
        phiac : angle of AC plane wrt. polarisation plane
    """

    # Warning: rotation of polarised state is not implemented yet!
    return DalitzPhaseSpace.FinalStateMomenta(self,m2ab, m2bc)

  def Dimensionality(self) : 
    return 5
