# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

#
# Example of toy MC generation in 5D phase space of four-body Lambda_b^0 -> p pi- pi+ pi- decay
# (no fit is performed here)
# The amplitude is a single scalar in p pi- channel. 
#

import tensorflow as tf

import sys, os
sys.path.append("..")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from TensorFlowAnalysis import *

from ROOT import TFile

if __name__ == "__main__" : 

  # Masses of initial and final state particles
  mp  = 0.938
  mpi = 0.139
  mk  = 0.497
  mlb = 5.620

  # Define phase space for fou-body C -> A1 A2 B1 B2 decay. It is a 5D phase space consisting of 
  # two invariant masses of A1 A2 and B1 B2 combination, as well as three angles: 
  # two helicity angles of A1 A2 and B1 B2 combinations, and the angle between A1 A2 and B1 B2 planes 
  # in the rest frame of C
  phsp = FourBodyHelicityPhaseSpace(mp, mpi, mpi, mpi, mlb)

  # Generate uniform sample
  uniform_sample = phsp.UniformSample(1000000)

  # Blatt-Weisskopf radial parameters
  dd = Const(5.)
  dr = Const(1.5)

  # Parameters of the intermediate resonance
  mass   = Const(1.232)
  width  = Const(0.117)

  ### Start of model description

  def model(x) : 
    # For the phase space vector x, this function returns 
    # four 4-momenta of the final state particles
    (pa1, pa2, pb1, pb2) = phsp.FinalStateMomenta(x)

    # Density of the phase space (it is not constant unlike in the 3-body Dalitz plot case)
    d = phsp.Density(x)

    # Invariant masses
    ma12 = Mass(pa1 + pa2)
    mb12 = Mass(pb1 + pb2)

    # Breit-Wigner amplitude
    ampl = BreitWignerLineShape(ma12**2, mass,  width, mp, mpi, mb12, mlb, dr, dd, 0, 0)

    # PDF is a product of phase space density and the abs value squared of the amplitude
    pdf = Density(ampl)*d
    return pdf

  ### End of model description

  # Initialise TF
  init = tf.global_variables_initializer()
  sess = tf.Session()
  sess.run(init)

  # Calculate maximum of the PDF for toy MC generation using accept-reject method
  majorant = EstimateMaximum(sess, model(phsp.data_placeholder), phsp.data_placeholder, sess.run( phsp.UniformSample(1000000) ) )*1.1
  print "Maximum = ", majorant

  # Generate toy MC sample
  data = RunToyMC( sess, model(phsp.data_placeholder), phsp.data_placeholder, phsp, 1000000, majorant, chunk = 1000000)

  # Store it to ROOT NTuple
  f = TFile.Open("toyresult.root", "RECREATE")
  FillNTuple("toy", data, ["m1", "m2", "cos1", "cos2", "phi" ] )
  f.Close()
